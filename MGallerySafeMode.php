<?php
/****************************************************************
* Aeva Media													*
* � noisen.com													*
*****************************************************************
* MGallerySafeMode.php - FTP account information for Safe Mode	*
*****************************************************************
* Users of this software are bound by the terms of the			*
* Aeva Media license. You can view it in the license_am.txt		*
* file, or online at http://noisen.com/license.php				*
*																*
* Support and updates for this software can be found at			*
* http://aeva.noisen.com and http://smf-media.com				*
****************************************************************/

// In case your server has PHP Safe Mode enabled by default,
// Aeva Media will need to create gallery folders using FTP.
// This file holds information about the FTP account needed for that.

// Once you entered your FTP account information, rename the file to
// something smart, go to the Admin > Aeva > Settings area, and enter
// the absolute file path to this new file. You're good to go.

if (!defined('SMF'))
	die();

global $context;

$context['smg_ftp'] = array();
// Enter your FTP account's server address
$context['smg_ftp']['server'] = 'localhost';
// Enter your FTP account's server port
$context['smg_ftp']['port'] = 21;
// Enter your FTP account's user name
$context['smg_ftp']['username'] = 'username';
// Enter your FTP account's password
$context['smg_ftp']['password'] = 'password';
// Enter the location of your data folder (usually mgal_data) inside your FTP file tree
// For example, if it is located in /home/myaccount/public_html/mgal_data,
// you would usually want to enter this: '/public_html/mgal_data'
// For best security, please make sure to create a special FTP account restricted
// to your data folder, and set $context['smg_ftp']['mgal_data'] to '/'
$context['smg_ftp']['mgal_data'] = '/mgal_data';

?>