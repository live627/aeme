<?php
/****************************************************************
* Aeva Media													*
* � Noisen.com & SMF-Media.com									*
*****************************************************************
* db_aeva.php - database installer								*
*****************************************************************
* Users of this software are bound by the terms of the			*
* Aeva Media license. You can view it in the license_am.txt		*
* file, or online at http://noisen.com/license.php				*
*																*
* Support and updates for this software can be found at			*
* http://aeva.noisen.com and http://smf-media.com				*
****************************************************************/

// This is the DB installer/upgrader for Aeva Media
// This runs standalone if it's in the same directory as SSI.php. It may also run via the Package Manager
// Does edits for both SMF 1.1 and SMF 2.0

// OK, first load the stuff
global
	$smcFunc, $db_prefix, $db_type, $db_name, $db_passwd, $db_user, $db_server,
	$context, $boarddir, $modSettings, $scripturl, $boardurl, $boarddir, $sourcedir;

$doing_manual_install = false;
$no_prefix = array('no_prefix' => true);
$primary_groups = array(0 => 0);

if (!defined('SMF') && file_exists(dirname(__FILE__) . '/SSI.php'))
{
	require_once(dirname(__FILE__) . '/SSI.php');
	$doing_manual_install = true;
}
elseif (!defined('SMF'))
	die('The installer wasn\'t able to connect to SMF! Make sure that you are either installing this via the Package Manager or the SSI.php file is in the same directory.');

if (isset($_GET['delete']))
{
	@unlink(__FILE__);

	// From SMF
	header('Location: http://' . (isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT']) . dirname($_SERVER['PHP_SELF']) . '/Themes/default/images/blank.gif');
	exit;
}

if (file_exists($boarddir . '/MGalleryItem.php'))
	@chmod($boarddir . '/MGalleryItem.php', 0644);

if ($doing_manual_install)
	echo '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
	<title>Aeva Media Database Installer</title>
	<link rel="stylesheet" type="text/css" href="Themes/default/style.css" />
	<link rel="stylesheet" type="text/css" href="Themes/default/css/index.css" />
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body>
	<br /><br />';

$ind = file($boarddir . '/index.php');
foreach ($ind as $line)
	if (strpos($line, '$forum_version') !== false)
		break;
$vers = strpos($line, '$forum_version') !== false && preg_match('~\'SMF (.*?)\'~', $line, $match) ? $match[1] : '1';

// Step 1 -- Install Aeva auto-embedder variables
$update = array(
	'aeva_enable' => 1, // Enabled by default
	'aeva_lookups' => 1,
	'aeva_max_per_post' => 12,
	'aeva_max_per_page' => 12,
	'aeva_yq' => 0,
	'aeva_titles' => 0,
	'aeva_inlinetitles' => 1,
	'aeva_noscript' => 0,
	'aeva_expins' => 1,
	'aeva_quotes' => 0,
	'aeva_incontext' => 1,
	'aeva_fix_html' => 1,
	'aeva_includeurl' => 1,
	'aeva_debug' => 0,
	'aeva_adult' => 0,
	'aeva_nonlocal' => 0,
	'aeva_mp3' => 0,
	'aeva_flv' => 0,
	'aeva_avi' => 0,
	'aeva_divx' => 0,
	'aeva_mov' => 0,
	'aeva_wmp' => 0,
	'aeva_real' => 0,
	'aeva_swf' => 0,
);

$context['is_smf2'] = isset($smcFunc) && function_exists('db_extend');

if ($context['is_smf2'])
{
	foreach ($update as $name => $value)
		$smcFunc['db_insert']('ignore', $db_prefix . 'settings', array('variable' => 'string', 'value' => 'string'), array($name, $value), '');

	$smcFunc['db_query']('', "DELETE FROM {db_prefix}settings WHERE (variable LIKE 'aevac_%') OR (variable IN ('aeva_quicktime', 'aeva_windowsmedia', 'aeva_realmedia', 'aeva_flash', 'aeva_youtube', 'aeva_ytitles', 'aeva_hq', 'aeva_quality', 'aeva_nossi', 'aeva_copyright', 'aeva_latest_version', 'aeva_version_test'))");
}
elseif (function_exists('mysql_query'))
{
	foreach ($update as $name => $value)
		mysql_query("INSERT IGNORE INTO {$db_prefix}settings (variable, value) VALUES ('$name', '$value')");

	mysql_query("DELETE FROM {$db_prefix}settings WHERE (variable LIKE 'aevac_%') OR (variable IN ('aeva_quicktime', 'aeva_windowsmedia', 'aeva_realmedia', 'aeva_flash', 'aeva_youtube', 'aeva_ytitles', 'aeva_hq', 'aeva_quality', 'aeva_nossi', 'aeva_copyright', 'aeva_latest_version', 'aeva_version_test'))");
}

// Some variables
$aevaprefix = substr($vers, 0, 1) == '1' || substr($vers, 0, 8) == '2.0 Beta' || substr($vers, 0, 7) == '2.0 RC1' ? $db_prefix . 'aeva_' : '{db_prefix}aeva_';
// The new settings
$newsettings = array(
	'installed_on' => time(),
	'data_dir_path' => $boarddir . '/mgal_data',
	'data_dir_url' => $boardurl . '/mgal_data',
	'max_dir_files' => '150',
	'num_items_per_page' => '15',
	'num_items_per_line' => '5',
	'num_items_per_line_ext' => '5',
	'max_dir_size' => '51400',
	'max_file_size' => '1024',
	'max_width' => '1024',
	'max_height' => '1024',
	'allow_over_max' => '1',
	'upload_security_check' => '0',
	'jpeg_compression' => '80',
	'num_unapproved_items' => '0',
	'num_unapproved_albums' => '0',
	'num_unapproved_comments' => '0',
	'num_unapproved_item_edits' => '0',
	'num_unapproved_album_edits' => '0',
	'num_reported_items' => '0',
	'num_reported_comments' => '0',
	'recent_item_limit' => '5',
	'random_item_limit' => '5',
	'recent_comments_limit' => '10',
	'recent_albums_limit' => '10',
	'total_items' => '0',
	'total_comments' => '0',
	'total_albums' => '0',
	'total_contests' => '0',
	'show_sub_albums_on_index' => '1',
	'enable_re-rating' => '0',
	'use_exif_date' => '1',
	'max_thumb_width' => '120',
	'max_thumb_height' => '120',
	'max_preview_width' => '500',
	'max_preview_height' => '500',
	'max_bigicon_width' => '200',
	'max_bigicon_height' => '200',
	'max_thumbs_per_page' => '100',
	'max_title_length' => '30',
	'show_extra_info' => '1',
	'entities_convert' => '0',
	'image_handler' => 1,
	'enable_cache' => 0,
	'use_lightbox' => 1,
	'show_linking_code' => 1,
	'album_edit_unapprove' => 1,
	'item_edit_unapprove' => 1,
	'album_columns' => '1',
	'my_docs' => 'txt,rtf,pdf,xls,doc,ppt,docx,xlsx,pptx,xml,html,htm,php,css,js,zip,rar,ace,arj,7z,gz,tar,tgz,bz,bzip2,sit',
);
$altered_mem = array();
$table_names = array('albums', 'comments', 'fields', 'field_data', 'files', 'log_media', 'log_ratings', 'media', 'perms', 'quotas', 'settings', 'variables');

// Start it!
if ($context['is_smf2'])
{
	// SMF 2 installer
	// First load SMF 2's extra DB functions
	db_extend('packages');
	db_extend('extra');

	// Get the table list
	$tables = array();
	$tmp = $smcFunc['db_list_tables']();
	foreach ($tmp as $t)
		if (substr($db_prefix, 0, strlen($db_name) + 3) != '`' . $db_name . '`.')
			$tables[] = $t;
		else
			$tables[] = '`' . $db_name . '`.' . $t;

	foreach ($table_names as $name)
		if (!in_array($db_prefix . 'aeva_' . $name, $tables) && in_array($db_prefix . 'mgallery_' . $name, $tables))
			$smcFunc['db_query']('', 'ALTER TABLE {db_prefix}mgallery_' . $name . ' RENAME TO {db_prefix}aeva_' . $name, array());
	if (!in_array($db_prefix . 'aeva_playlists', $tables) && in_array($db_prefix . 'mgallery_playlists', $tables))
		$smcFunc['db_query']('', 'ALTER TABLE {db_prefix}mgallery_playlists RENAME TO {db_prefix}aeva_playlists', array());
	if (!in_array($db_prefix . 'aeva_playlist_data', $tables) && in_array($db_prefix . 'mgallery_playlist_data', $tables))
		$smcFunc['db_query']('', 'ALTER TABLE {db_prefix}mgallery_playlist_data RENAME TO {db_prefix}aeva_playlist_data', array());

	$smcFunc['db_query']('', '
		UPDATE IGNORE {db_prefix}permissions
		SET permission = REPLACE(permission, {string:mgallery}, {string:aeva})
	', array('mgallery' => 'mgallery_', 'aeva' => 'aeva_'));

	// Create the tables

	// The aeva_media table
	$smcFunc['db_create_table'](
		$aevaprefix . 'media',
		array(
			array('name' => 'id_media', 'type' => 'INT', 'auto' => true),
			array('name' => 'id_member', 'type' => 'INT', 'default' => 0),
			array('name' => 'member_name', 'type' => 'VARCHAR', 'default' => '', 'size' => '25'),
			array('name' => 'last_edited', 'type' => 'INT', 'default' => 0),
			array('name' => 'last_edited_by', 'type' => 'INT', 'default' => 0),
			array('name' => 'last_edited_name', 'type' => 'TEXT'),
			array('name' => 'id_file', 'type' => 'INT', 'default' => 0),
			array('name' => 'id_thumb', 'type' => 'INT', 'default' => 0),
			array('name' => 'id_preview', 'type' => 'INT', 'default' => 0),
			array('name' => 'type', 'type' => 'VARCHAR', 'size' => '10', 'default' => 'image'),
			array('name' => 'album_id', 'type' => 'INT', 'default' => 0),
			array('name' => 'rating', 'type' => 'INT', 'default' => 0),
			array('name' => 'voters', 'type' => 'MEDIUMINT', 'default' => 0),
			array('name' => 'weighted', 'type' => 'FLOAT', 'default' => 0),
			array('name' => 'title', 'type' => 'VARCHAR', 'size' => '255', 'default' => '(No title)'),
			array('name' => 'description', 'type' => 'TEXT'),
			array('name' => 'approved', 'type' => 'TINYINT', 'size' => '1', 'default' => '0'),
			array('name' => 'time_added', 'type' => 'INT', 'default' => '0'),
			array('name' => 'views', 'type' => 'INT', 'default' => '0'),
			array('name' => 'downloads', 'type' => 'INT', 'default' => '0'),
			array('name' => 'last_viewed', 'type' => 'TINYINT', 'size' => '1', 'default' => '0'),
			array('name' => 'keywords', 'type' => 'TEXT'),
			array('name' => 'embed_url', 'type' => 'TEXT'),
			array('name' => 'id_last_comment', 'type' => 'INT', 'default' => '0'),
			array('name' => 'log_last_access_time', 'type' => 'INT', 'default' => '0'),
			array('name' => 'num_comments', 'type' => 'INT', 'default' => '0'),
		),
		array(
			array(
				'type' => 'primary',
				'columns' => array('id_media')
			)
		),
		$no_prefix,
		'ignore'
	);
	$created_tables[] = $aevaprefix . 'media';

	// The aeva_files table
	$smcFunc['db_create_table'](
		$aevaprefix . 'files',
		array(
			array('name' => 'id_file', 'type' => 'INT', 'auto' => true),
			array('name' => 'filesize', 'type' => 'INT', 'size' => '20', 'default' => '0'),
			array('name' => 'filename', 'type' => 'TEXT'),
			array('name' => 'width', 'type' => 'INT', 'default' => '1', 'size' => '4'),
			array('name' => 'height', 'type' => 'INT', 'default' => '1', 'size' => '4'),
			array('name' => 'directory', 'type' => 'TEXT'),
			array('name' => 'id_album', 'type' => 'INT', 'default' => '0', 'size' => '20'),
			array('name' => 'exif', 'type' => 'TEXT'),
		),
		array(
			array(
				'type' => 'primary',
				'columns' => array('id_file')
			)
		),
		$no_prefix,
		'ignore'
	);
	$created_tables[] = $aevaprefix . 'files';

	// The aeva_albums table
	$smcFunc['db_create_table'](
		$aevaprefix . 'albums',
		array(
			array('name' => 'id_album', 'type' => 'INT', 'auto' => true),
			array('name' => 'album_of', 'type' => 'INT', 'default' => '0'),
			array('name' => 'featured', 'type' => 'TINYINT', 'size' => '1', 'default' => '0'),
			array('name' => 'name', 'type' => 'VARCHAR', 'size' => '255', 'default' => ''),
			array('name' => 'description', 'type' => 'TEXT'),
			array('name' => 'master', 'type' => 'INT', 'default' => '0'),
			array('name' => 'icon', 'type' => 'INT', 'default' => '0'),
			array('name' => 'bigicon', 'type' => 'INT', 'default' => '0'),
			array('name' => 'passwd', 'type' => 'VARCHAR', 'size' => '64', 'default' => ''),
			array('name' => 'directory', 'type' => 'TEXT'),
			array('name' => 'parent', 'type' => 'INT', 'default' => '0'),
			array('name' => 'access', 'type' => 'VARCHAR', 'size' => '255', 'default' => ''),
			array('name' => 'access_write', 'type' => 'VARCHAR', 'size' => '255', 'default' => ''),
			array('name' => 'approved', 'type' => 'TINYINT', 'size' => '1', 'default' => '0'),
			array('name' => 'a_order', 'type' => 'INT', 'default' => '0'),
			array('name' => 'child_level', 'type' => 'INT', 'default' => '0'),
			array('name' => 'id_last_media', 'type' => 'INT', 'default' => '0'),
			array('name' => 'num_items', 'type' => 'INT', 'default' => '0'),
			array('name' => 'options', 'type' => 'TEXT'),
			array('name' => 'id_perm_profile', 'type' => 'INT', 'default' => 0),
			array('name' => 'id_quota_profile', 'type' => 'INT', 'default' => 0),
			array('name' => 'hidden', 'type' => 'TINYINT', 'size' => '1', 'default' => '0'),
			array('name' => 'allowed_members', 'type' => 'VARCHAR', 'size' => '255', 'default' => ''),
			array('name' => 'allowed_write', 'type' => 'VARCHAR', 'size' => '255', 'default' => ''),
			array('name' => 'denied_members', 'type' => 'VARCHAR', 'size' => '255', 'default' => ''),
			array('name' => 'denied_write', 'type' => 'VARCHAR', 'size' => '255', 'default' => ''),
			array('name' => 'id_topic', 'type' => 'INT', 'default' => '0'),
		),
		array(
			array(
				'type' => 'primary',
				'columns' => array('id_album')
			),
			array(
				'columns' => array('album_of')
			),
			array(
				'columns' => array('id_album', 'album_of', 'featured')
			)
		),
		$no_prefix,
		'ignore'
	);
	$created_tables[] = $aevaprefix . 'albums';

	// The aeva_settings table
	$smcFunc['db_create_table'](
		$aevaprefix . 'settings',
		array(
			array('name' => 'name', 'type' => 'VARCHAR', 'size' => '30', 'default' => ''),
			array('name' => 'value', 'type' => 'TEXT'),
		),
		array(
			array(
				'type' => 'primary',
				'columns' => array('name')
			)
		),
		$no_prefix,
		'ignore'
	);
	$created_tables[] = $aevaprefix . 'settings';

	// The aeva_variables table
	$smcFunc['db_create_table'](
		$aevaprefix . 'variables',
		array(
			array('name' => 'id', 'type' => 'INT', 'auto' => true),
			array('name' => 'type', 'type' => 'VARCHAR', 'size' => '15', 'default' => ''),
			array('name' => 'val1', 'type' => 'TEXT'),
			array('name' => 'val2', 'type' => 'TEXT'),
			array('name' => 'val3', 'type' => 'TEXT'),
			array('name' => 'val4', 'type' => 'TEXT'),
			array('name' => 'val5', 'type' => 'TEXT'),
			array('name' => 'val6', 'type' => 'TEXT'),
			array('name' => 'val7', 'type' => 'TEXT'),
			array('name' => 'val8', 'type' => 'TEXT'),
			array('name' => 'val9', 'type' => 'TEXT'),
		),
		array(
			array(
				'type' => 'primary',
				'columns' => array('id')
			)
		),
		$no_prefix,
		'ignore'
	);
	$created_tables[] = $aevaprefix . 'variables';

	// The aeva_comments table
	$smcFunc['db_create_table'](
		$aevaprefix . 'comments',
		array(
			array('name' => 'id_comment', 'type' => 'INT', 'default' => '', 'auto' => true),
			array('name' => 'id_member', 'type' => 'INT', 'default' => '0'),
			array('name' => 'id_media', 'type' => 'INT', 'default' => '0'),
			array('name' => 'id_album', 'type' => 'INT', 'default' => '0'),
			array('name' => 'message', 'type' => 'TEXT'),
			array('name' => 'posted_on', 'type' => 'INT', 'default' => '0'),
			array('name' => 'last_edited', 'type' => 'INT', 'default' => '0'),
			array('name' => 'last_edited_by', 'type' => 'INT', 'default' => '0'),
			array('name' => 'last_edited_name', 'type' => 'VARCHAR', 'size' => '25', 'default' => ''),
			array('name' => 'approved', 'type' => 'TINYINT', 'size' => '1', 'default' => '0'),
		),
		array(
			array(
				'type' => 'primary',
				'columns' => array('id_comment')
			)
		),
		$no_prefix,
		'ignore'
	);
	$created_tables[] = $aevaprefix . 'comments';

	// The aeva_log_media table
	$smcFunc['db_create_table'](
		$aevaprefix . 'log_media',
		array(
			array('name' => 'id_media', 'type' => 'INT', 'default' => '0'),
			array('name' => 'id_member', 'type' => 'INT', 'default' => '0'),
			array('name' => 'time', 'type' => 'INT', 'default' => '0'),
		),
		array(
			array(
				'type' => 'primary',
				'columns' => array('id_media', 'id_member')
			)
		),
		$no_prefix,
		'ignore'
	);
	$created_tables[] = $aevaprefix . 'log_media';

	// The aeva_log_ratings table
	$smcFunc['db_create_table'](
		$aevaprefix . 'log_ratings',
		array(
			array('name' => 'id_media', 'type' => 'INT', 'default' => '0'),
			array('name' => 'id_member', 'type' => 'INT', 'default' => '0'),
			array('name' => 'rating', 'type' => 'INT', 'default' => '0'),
			array('name' => 'time', 'type' => 'INT', 'default' => '0')
		),
		array(
			array(
				'type' => 'primary',
				'columns' => array('id_media', 'id_member'),
			)
		),
		$no_prefix,
		'ignore'
	);
	$created_tables[] = $aevaprefix . 'log_ratings';

	// The permissions table
	$smcFunc['db_create_table'](
		$aevaprefix . 'perms',
		array(
			array('name' => 'id_group', 'type' => 'INT', 'default' => 0),
			array('name' => 'id_profile', 'type' => 'INT', 'default' => 0),
			array('name' => 'permission', 'type' => 'VARCHAR', 'size' => 255, 'default' => ''),
		),
		array(
			array(
				'type' => 'primary',
				'columns' => array('id_group', 'id_profile', 'permission'),
			),
		),
		$no_prefix,
		'ignore'
	);
	$created_tables[] = $aevaprefix . 'perms';

	// The member group quotas table
	$smcFunc['db_create_table'](
		$aevaprefix . 'quotas',
		array(
			array('name' => 'id_profile', 'type' => 'INT', 'default' => 0),
			array('name' => 'id_group', 'type' => 'INT', 'default' => 0),
			array('name' => 'type', 'type' => 'VARCHAR', 'size' => 10, 'default' => ''),
			array('name' => 'quota', 'type' => 'INT', 'default' => 0),
		),
		array(
			array(
				'type' => 'primary',
				'columns' => array('id_profile', 'id_group', 'type'),
			),
		),
		$no_prefix,
		'ignore'
	);
	$created_tables[] = $aevaprefix . 'quotas';

	// The custom fields table
	$smcFunc['db_create_table'](
		$aevaprefix . 'fields',
		array(
			array('name' => 'id_field', 'type' => 'INT', 'auto' => true, 'default' => 0),
			array('name' => 'name', 'type' => 'VARCHAR', 'size' => 100, 'default' => ''),
			array('name' => 'type', 'type' => 'VARCHAR', 'size' => 20, 'default' => 'text'),
			array('name' => 'options', 'type' => 'TEXT'),
			array('name' => 'required', 'type' => 'TINYINT', 'size' => 1, 'default' => 0),
			array('name' => 'searchable', 'type' => 'TINYINT', 'size' => 1, 'default' => 0),
			array('name' => 'description', 'type' => 'TEXT'),
			array('name' => 'bbc', 'type' => 'TINYINT', 'size' => 1, 'default' => 0),
			array('name' => 'albums', 'type' => 'TEXT'),
		),
		array(
			array(
				'type' => 'primary',
				'columns' => array('id_field'),
			),
		),
		$no_prefix,
		'ignore'
	);
	$created_tables[] = $aevaprefix . 'fields';

	// The custom field's data table
	$smcFunc['db_create_table'](
		$aevaprefix . 'field_data',
		array(
			array('name' => 'id_field', 'type' => 'INT', 'default' => 0),
			array('name' => 'id_media', 'type' => 'INT', 'default' => 0),
			array('name' => 'value', 'type' => 'TEXT'),
		),
		array(
			array(
				'type' => 'primary',
				'columns' => array('id_field', 'id_media'),
			),
		),
		$no_prefix,
		'ignore'
	);
	$created_tables[] = $aevaprefix . 'field_data';

	$mem_columns = $smcFunc['db_list_columns']($db_prefix . 'members', false, $no_prefix);
	$file_columns = $smcFunc['db_list_columns']($aevaprefix . 'files', false, $no_prefix);
	$album_columns = $smcFunc['db_list_columns']($aevaprefix . 'albums', false, $no_prefix);
	$media_columns = $smcFunc['db_list_columns']($aevaprefix . 'media', false, $no_prefix);
	$field_columns = $smcFunc['db_list_columns']($aevaprefix . 'fields', false, $no_prefix);
	$quota_columns = $smcFunc['db_list_columns']($aevaprefix . 'quotas', false, $no_prefix);

	if (!in_array('description', $field_columns))
		$smcFunc['db_query']('', 'ALTER TABLE ' . $aevaprefix . 'fields CHANGE `desc` description TEXT NOT NULL', array());
	if (!in_array('quota', $quota_columns))
		$smcFunc['db_query']('', 'ALTER TABLE ' . $aevaprefix . 'quotas CHANGE `limit` quota INT NOT NULL DEFAULT 0', array());

	if (!in_array('id_perm_profile', $album_columns))
		$smcFunc['db_add_column']($aevaprefix . 'albums', array('name' => 'id_perm_profile', 'type' => 'INT', 'default' => 0), $no_prefix);
	if (!in_array('id_quota_profile', $album_columns))
		$smcFunc['db_add_column']($aevaprefix . 'albums', array('name' => 'id_quota_profile', 'type' => 'INT', 'default' => 0), $no_prefix);
	if (!in_array('hidden', $album_columns))
		$smcFunc['db_add_column']($aevaprefix . 'albums', array('name' => 'hidden', 'type' => 'TINYINT', 'size' => '1', 'default' => 0), $no_prefix);
	if (!in_array('allowed_members', $album_columns))
		$smcFunc['db_add_column']($aevaprefix . 'albums', array('name' => 'allowed_members', 'type' => 'VARCHAR', 'size' => '255', 'default' => ''), $no_prefix);
	if (!in_array('allowed_write', $album_columns))
		$smcFunc['db_add_column']($aevaprefix . 'albums', array('name' => 'allowed_write', 'type' => 'VARCHAR', 'size' => '255', 'default' => ''), $no_prefix);
	if (!in_array('denied_members', $album_columns))
		$smcFunc['db_add_column']($aevaprefix . 'albums', array('name' => 'denied_members', 'type' => 'VARCHAR', 'size' => '255', 'default' => ''), $no_prefix);
	if (!in_array('denied_write', $album_columns))
		$smcFunc['db_add_column']($aevaprefix . 'albums', array('name' => 'denied_write', 'type' => 'VARCHAR', 'size' => '255', 'default' => ''), $no_prefix);
	if (!in_array('id_topic', $album_columns))
		$smcFunc['db_add_column']($aevaprefix . 'albums', array('name' => 'id_topic', 'type' => 'INT', 'default' => 0), $no_prefix);
	if (!in_array('bigicon', $album_columns))
		$smcFunc['db_add_column']($aevaprefix . 'albums', array('name' => 'bigicon', 'type' => 'INT', 'default' => 0), $no_prefix);
	if (!in_array('access_write', $album_columns))
		$smcFunc['db_add_column']($aevaprefix . 'albums', array('name' => 'access_write', 'type' => 'VARCHAR', 'size' => '255', 'default' => ''), $no_prefix);
	if (!in_array('master', $album_columns))
	{
		$smcFunc['db_add_column']($aevaprefix . 'albums', array('name' => 'master', 'type' => 'INT', 'default' => 0), $no_prefix);
		$smcFunc['db_add_index']($aevaprefix . 'albums', array('name' => 'id_master', 'columns' => array('master')), $no_prefix);
		$smcFunc['db_query']('', 'UPDATE ' . $aevaprefix . 'albums SET master = id_album WHERE parent = 0', array());
		$alb = array();
		$continue = true;
		$unstick = 0;
		while ($continue)
		{
			// This may very well crash on SQLite and maybe PGSQL... Ah, who cares?
			$smcFunc['db_query']('', '
				UPDATE ' . $aevaprefix . 'albums AS a1, ' . $aevaprefix . 'albums AS a2
				SET a1.master = a2.master
				WHERE (a1.parent = a2.id_album) AND (a1.master = 0) AND (a2.master != 0)',
				array());
			$continue = ($smcFunc['db_affected_rows']() > 0) && ($unstick++ < 100);
		}
	}
	if (!in_array('featured', $album_columns))
	{
		// Retrieve all non-admin primary groups used by members...
		$request = $smcFunc['db_query']('', 'SELECT id_group FROM {db_prefix}members GROUP BY id_group ORDER BY id_group', array());
		while ($row = $smcFunc['db_fetch_row']($request))
			$primary_groups[(int) $row[0]] = (int) $row[0];
		$smcFunc['db_free_result']($request);
		unset($primary_groups[1]);

		$smcFunc['db_add_column']($aevaprefix . 'albums', array('name' => 'featured', 'type' => 'TINYINT', 'size' => '1', 'default' => '0'), $no_prefix);
		$smcFunc['db_query']('', '
			UPDATE ' . $aevaprefix . 'albums
			SET featured = 1, album_of = 1, access_write = {string:write}
			WHERE type = {string:general}',
			array(
				'general' => 'general',
				'write' => implode(',', array_keys($primary_groups)),
			)
		);
		$smcFunc['db_add_index']($aevaprefix . 'albums', array('name' => 'id_of', 'columns' => array('id_album', 'album_of', 'featured')), $no_prefix);
		$smcFunc['db_remove_column']($aevaprefix . 'albums', 'type', $no_prefix);
	}
	else
		$smcFunc['db_query']('', 'UPDATE ' . $aevaprefix . 'albums SET album_of = 1 WHERE album_of = 0', array());
	if (in_array('member_name', $album_columns))
		$smcFunc['db_remove_column']($aevaprefix . 'albums', 'member_name', $no_prefix);

	// If mgal_* fields are in there, rename them. Otherwise, just create the aeva_* fields.
	if (!in_array('aeva_items', $mem_columns))
	{
		if (in_array('mgal_total_items', $mem_columns))
			$smcFunc['db_change_column']($db_prefix . 'members', 'mgal_total_items', array('name' => 'aeva_items', 'type' => 'INT', 'null' => null, 'default' => 0), $no_prefix);
		else
			$smcFunc['db_add_column']($db_prefix . 'members', array('name' => 'aeva_items', 'type' => 'INT', 'null' => null, 'default' => '0'), $no_prefix);
		$altered_mem[1] = true;
	}
	if (!in_array('aeva_comments', $mem_columns))
	{
		if (in_array('mgal_total_comments', $mem_columns))
			$smcFunc['db_change_column']($db_prefix . 'members', 'mgal_total_comments', array('name' => 'aeva_comments', 'type' => 'INT', 'null' => null, 'default' => 0), $no_prefix);
		else
			$smcFunc['db_add_column']($db_prefix . 'members', array('name' => 'aeva_comments', 'type' => 'INT', 'null' => null, 'default' => '0'), $no_prefix);
		$altered_mem[2] = true;
	}
	if (!in_array('aeva_unseen', $mem_columns))
	{
		if (in_array('mgal_unseen', $mem_columns))
			$smcFunc['db_change_column']($db_prefix . 'members', 'mgal_unseen', array('name' => 'aeva_unseen', 'type' => 'INT', 'null' => null, 'default' => -1), $no_prefix);
		else
			$smcFunc['db_add_column']($db_prefix . 'members', array('name' => 'aeva_unseen', 'type' => 'INT', 'null' => null, 'default' => '-1'), $no_prefix);
		$altered_mem[3] = true;
	}

	// I'd rather use a TEXT field, but if SELECT @@sql_mode returns a strict mode, it may cause issues outside the mod itself...
	if (!in_array('aeva', $mem_columns))
	{
		$smcFunc['db_add_column']($db_prefix . 'members', array('name' => 'aeva', 'type' => 'VARCHAR', 'size' => '255', 'null' => null, 'default' => ''), $no_prefix);
		$altered_mem[4] = true;
	}

	if (!in_array('exif', $file_columns))
		$smcFunc['db_add_column']($aevaprefix . 'files', array('name' => 'exif', 'type' => 'TEXT'), $no_prefix);
	if (!in_array('options', $album_columns))
		$smcFunc['db_add_column']($aevaprefix . 'albums', array('name' => 'options', 'type' => 'TEXT'), $no_prefix);
	if (!in_array('id_preview', $media_columns))
	{
		$smcFunc['db_add_column']($aevaprefix . 'media', array('name' => 'id_preview', 'type' => 'INT', 'default' => '0'), $no_prefix);
		$smcFunc['db_change_column']($aevaprefix . 'media', 'type', array('type' => 'VARCHAR', 'size' => '10', 'default' => 'image'), $no_prefix);
	}
	if (!in_array('downloads', $media_columns))
	{
		$smcFunc['db_add_column']($aevaprefix . 'media', array('name' => 'downloads', 'type' => 'INT', 'default' => '0'), $no_prefix);
		$smcFunc['db_change_column']($aevaprefix . 'albums', 'access', array('size' => 255), $no_prefix);
	}
	if (!in_array('weighted', $media_columns))
		$smcFunc['db_add_column']($aevaprefix . 'media', array('name' => 'weighted', 'type' => 'FLOAT', 'default' => '0'), $no_prefix);
	$smcFunc['db_change_column']($aevaprefix . 'media', 'title', array('size' => 255), $no_prefix);
	$smcFunc['db_change_column']($aevaprefix . 'albums', 'name', array('size' => 255), $no_prefix);
	$smcFunc['db_change_column']($aevaprefix . 'settings', 'value', array('type' => 'TEXT'), $no_prefix);

	$media_keys = $smcFunc['db_list_indexes']($aevaprefix . 'media', false, $no_prefix);
	// id_thumb index is needed for the Check Orphans maintenance task.
	if (!in_array('id_thumb', $media_keys))
		$smcFunc['db_add_index']($aevaprefix . 'media', array('name' => 'id_thumb', 'columns' => array('id_thumb')), $no_prefix);
	if (!in_array('time_added', $media_keys))
		$smcFunc['db_add_index']($aevaprefix . 'media', array('name' => 'time_added', 'columns' => array('time_added')), $no_prefix);
	if (!in_array('album_id', $media_keys))
		$smcFunc['db_add_index']($aevaprefix . 'media', array('name' => 'album_id', 'columns' => array('album_id')), $no_prefix);

	// Permissions processing...
	if (!in_array($db_prefix . 'aeva_perms', $tables) && !in_array($db_prefix . 'mgallery_perms', $tables))
	{
		// Insert a brand new profile
		$smcFunc['db_insert'](
			'ignore',
			'{db_prefix}aeva_variables',
			array(
				'type' => 'string',
				'val1' => 'string',
			),
			array(
				'perm_profile',
				'Default',
			),
			array('id')
		);
		$id_profile = $smcFunc['db_insert_id']('{db_prefix}aeva_variables');

		// Bypass to a small issue
		if ($id_profile == 1)
		{
			$id_profile = 2;
			$smcFunc['db_query']('', '
				UPDATE {db_prefix}aeva_variables
				SET id = 2
				WHERE id = 1',
				array()
			);
			// I think only MySQL supports the AUTO_INCREMENT setting.
			if (!empty($db_type) && $db_type === 'mysql')
				$smcFunc['db_query']('', '
					ALTER TABLE {db_prefix}aeva_variables AUTO_INCREMENT = 3', array());
		}

		// Get existing permissions
		$request = $smcFunc['db_query']('', '
			SELECT permission, add_deny, id_group
			FROM {db_prefix}permissions',
			array()
		);
		$removals = array();
		$perms = array();
		while ($row = $smcFunc['db_fetch_assoc']($request))
		{
			if (!in_array($row['permission'], array('aeva_download_item', 'aeva_add_videos', 'aeva_add_audios', 'aeva_add_images', 'aeva_add_embeds', 'aeva_add_docs', 'aeva_rate_items', 'aeva_edit_own_com', 'aeva_edit_own_item', 'aeva_comment', 'aeva_report_item', 'aeva_report_com', 'aeva_auto_approve_com', 'aeva_auto_approve_item', 'aeva_multi_upload', 'aeva_multi_download', 'aeva_whoratedwhat')))
				continue;

			if (!isset($perms[$row['id_group']]))
				$perms[$row['id_group']] = array();
			if (!isset($removals[$row['id_group']]))
				$removals[$row['id_group']] = array();

			if (empty($row['add_deny']))
				$removals[$row['id_group']][] = substr($row['permission'], 9);
			else
				$perms[$row['id_group']][] = substr($row['permission'], 9);
		}
		$smcFunc['db_free_result']($request);

		if (!empty($modSettings['permission_enable_deny']))
			foreach ($perms as $group => $permarray)
				$perms[$group] = array_diff($perms[$group], $removals[$group]);

		// Insert it to the profile
		foreach ($perms as $group => $permArray)
			foreach ($permArray as $perm)
				$smcFunc['db_insert']('ignore',
					'{db_prefix}aeva_perms',
					array(
						'id_profile' => 'int',
						'id_group' => 'int',
						'permission' => 'string',
					),
					array(
						$id_profile,
						$group,
						$perm,
					),
					array('id_profile', 'id_group', 'permission')
				);

		// Update the albums - old style, so we're considering featured albums as 'general'
		$smcFunc['db_query']('', '
			UPDATE {db_prefix}aeva_albums
			SET id_perm_profile = {int:profile}
			WHERE featured = 1',
			array(
				'profile' => $id_profile,
			)
		);
		$smcFunc['db_query']('', '
			UPDATE {db_prefix}aeva_albums
			SET id_perm_profile = 1
			WHERE featured = 0',
			array()
		);
	}
	// Import the settings now
	$setting_entries = array();
	foreach ($newsettings as $name => $value)
	{
		$smcFunc['db_insert']('ignore', $aevaprefix . 'settings', array('name' => 'string', 'value' => 'string'), array($name, $value), '');
		if ($smcFunc['db_affected_rows']() > 0)
			$setting_entries[] = $name;
	}

	// Insert the mandatory data
	$smcFunc['db_query']('', 'DELETE FROM {db_prefix}aeva_files WHERE id_file <= 4');
	$smcFunc['db_query']('', 'DELETE FROM {db_prefix}aeva_settings WHERE name = \'doc_files\'');
	$smcFunc['db_query']('', 'DELETE FROM {db_prefix}aeva_settings WHERE name = \'version\'');

	$smcFunc['db_insert']('',
		$aevaprefix . 'files',
		array('id_file' => 'int', 'filename' => 'string-255', 'filesize' => 'int', 'directory' => 'string-255', 'width' => 'int', 'height' => 'int', 'id_album' => 'int', 'exif' => 'string-255'),
		array(1, 'music.png', 4118, 'generic_images', 48, 48, 0, ''),
		array('id_file')
	);
	$smcFunc['db_insert']('',
		$aevaprefix . 'files',
		array('id_file' => 'int', 'filename' => 'string-255', 'filesize' => 'int', 'directory' => 'string-255', 'width' => 'int', 'height' => 'int', 'id_album' => 'int', 'exif' => 'string-255'),
		array(2, 'film.png', 2911, 'generic_images', 48, 48, 0, ''),
		array('id_file')
	);
	$smcFunc['db_insert']('',
		$aevaprefix . 'files',
		array('id_file' => 'int', 'filename' => 'string-255', 'filesize' => 'int', 'directory' => 'string-255', 'width' => 'int', 'height' => 'int', 'id_album' => 'int', 'exif' => 'string-255'),
		array(3, 'camera.png', 2438, 'generic_images', 48, 48, 0, ''),
		array('id_file')
	);
	$smcFunc['db_insert']('',
		$aevaprefix . 'files',
		array('id_file' => 'int', 'filename' => 'string-255', 'filesize' => 'int', 'directory' => 'string-255', 'width' => 'int', 'height' => 'int', 'id_album' => 'int', 'exif' => 'string-255'),
		array(4, 'folder.png', 2799, 'generic_images', 48, 48, 0, ''),
		array('id_file')
	);

	$request = $smcFunc['db_query']('', '
		SELECT value FROM {db_prefix}aeva_settings WHERE name = {string:data_dir}',
		array('data_dir' => 'data_dir_path'));
	list($data_dir) = $smcFunc['db_fetch_row']($request);
	$data_dir .= '/generic_images/';
	$ex_data_dir = $boarddir . '/mgal_data/generic_images/';
	$cam = $data_dir . 'camera.png';
	if ((!file_exists($cam) || filesize($cam) == 665) && filesize($ex_data_dir . 'camera.png') == 2438)
	{
		@copy($ex_data_dir . 'camera.png', $data_dir . 'camera.png');
		@copy($ex_data_dir . 'film.png', $data_dir . 'film.png');
		@copy($ex_data_dir . 'music.png', $data_dir . 'music.png');
		@copy($ex_data_dir . 'folder.png', $data_dir . 'folder.png');
	}
	$smcFunc['db_free_result']($request);

	if (file_exists($sourcedir . '/Aeva-Subs-Vital.php'))
	{
		require_once($sourcedir . '/Aeva-Subs-Vital.php');
		if (function_exists('aeva_allowed_types'))
		{
			$aty = aeva_allowed_types();
			$aty['do'][] = 'default';
			foreach ($aty['do'] as $ty)
				if (!file_exists($data_dir . $ty . '.png') && file_exists($ex_data_dir . $ty . '.png'))
					@copy($ex_data_dir . $ty . '.png', $data_dir . $ty . '.png');
		}
	}
}
else
{
	// SMF 1.1 installer
	// First list all the tables
	$request = db_query("
		SHOW TABLES
		FROM `$db_name`"
	,__FILE__,__LINE__);
	$tables = array();
	while ($row = mysql_fetch_row($request))
		if (substr($db_prefix,0,strlen($db_name) + 3) != '`'.$db_name.'`.')
			$tables[] = $row[0];
		else
			$tables[] = '`'.$db_name . '`.'.$row[0];

	mysql_free_result($request);

	foreach ($table_names as $name)
		if (!in_array($db_prefix . 'aeva_' . $name, $tables) && in_array($db_prefix . 'mgallery_' . $name, $tables))
			db_query("ALTER TABLE {$db_prefix}mgallery_{$name} RENAME TO {$db_prefix}aeva_{$name}",__FILE__,__LINE__);
	if (!in_array($db_prefix . 'aeva_playlists', $tables) && in_array($db_prefix . 'mgallery_playlists', $tables))
		db_query("
			RENAME TABLE
				{$db_prefix}mgallery_playlists TO {$db_prefix}aeva_playlists,
				{$db_prefix}mgallery_playlist_data TO {$db_prefix}aeva_playlist_data
		",__FILE__,__LINE__);
	db_query("UPDATE IGNORE {$db_prefix}permissions SET permission = REPLACE(permission, 'mgallery_', 'aeva_')",__FILE__,__LINE__);

	// Create the tables

	// The aeva_media table
	db_query("
		CREATE TABLE IF NOT EXISTS {$aevaprefix}media (
			id_media INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
			id_member INT NOT NULL,
			member_name VARCHAR(25) NOT NULL DEFAULT '',
			last_edited INT NOT NULL DEFAULT 0,
			last_edited_by INT NOT NULL DEFAULT 0,
			last_edited_name TEXT NOT NULL,
			id_file INT NOT NULL DEFAULT 0,
			id_thumb INT NOT NULL DEFAULT 0,
			id_preview INT NOT NULL DEFAULT 0,
			type VARCHAR(10) NOT NULL DEFAULT 'image',
			album_id INT NOT NULL DEFAULT 0,
			rating INT NOT NULL DEFAULT 0,
			voters MEDIUMINT NOT NULL DEFAULT 0,
			weighted FLOAT NOT NULL DEFAULT 0,
			title VARCHAR(255) NOT NULL DEFAULT '(No title)',
			description TEXT NOT NULL,
			approved TINYINT(1) NOT NULL DEFAULT 0,
			time_added INT NOT NULL DEFAULT 0,
			views INT NOT NULL DEFAULT 0,
			downloads INT NOT NULL DEFAULT 0,
			last_viewed INT NOT NULL DEFAULT 0,
			keywords TEXT NOT NULL,
			embed_url TEXT NOT NULL,
			id_last_comment INT NOT NULL DEFAULT 0,
			log_last_access_time INT NOT NULL DEFAULT 0,
			num_comments INT NOT NULL DEFAULT 0
		)",__FILE__,__LINE__);

	$created_tables[] = $aevaprefix . 'media';

	// The aeva_files table
	db_query("
		CREATE TABLE IF NOT EXISTS {$aevaprefix}files (
			id_file INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
			filesize INT NOT NULL DEFAULT 0,
			filename TEXT NOT NULL,
			width INT NOT NULL DEFAULT 1,
			height INT NOT NULL DEFAULT 1,
			directory TEXT NOT NULL,
			id_album INT NOT NULL DEFAULT 0,
			exif TEXT NOT NULL
		)
	",__FILE__,__LINE__);

	$created_tables[] = $aevaprefix . 'files';

	// The aeva_albums table
	db_query("
		CREATE TABLE IF NOT EXISTS {$aevaprefix}albums (
			id_album INT AUTO_INCREMENT PRIMARY KEY NOT NULL,
			album_of INT NOT NULL DEFAULT 0,
			featured TINYINT(1) NOT NULL DEFAULT 0,
			name VARCHAR(255) NOT NULL DEFAULT '',
			description TEXT NOT NULL,
			master INT NOT NULL DEFAULT 0,
			icon INT NOT NULL DEFAULT 0,
			bigicon INT NOT NULL DEFAULT 0,
			passwd VARCHAR(64) NOT NULL DEFAULT '',
			directory TEXT NOT NULL,
			parent INT NOT NULL DEFAULT 0,
			access VARCHAR(255) NOT NULL DEFAULT '',
			access_write VARCHAR(255) NOT NULL DEFAULT '',
			approved TINYINT(1) NOT NULL DEFAULT 0,
			a_order INT NOT NULL DEFAULT 0,
			child_level INT NOT NULL DEFAULT 0,
			id_last_media INT NOT NULL DEFAULT 0,
			num_items INT NOT NULL DEFAULT 0,
			options TEXT NOT NULL,
			id_perm_profile INT NOT NULL DEFAULT 0,
			id_quota_profile INT NOT NULL DEFAULT 0,
			hidden TINYINT(1) NOT NULL DEFAULT 0,
			allowed_members VARCHAR(255) NOT NULL DEFAULT '',
			allowed_write VARCHAR(255) NOT NULL DEFAULT '',
			denied_members VARCHAR(255) NOT NULL DEFAULT '',
			denied_write VARCHAR(255) NOT NULL DEFAULT '',
			id_topic INT NOT NULL DEFAULT 0,
			KEY album_of (album_of),
			KEY id_of (id_album, album_of, featured)
		)
	",__FILE__,__LINE__);

	$created_tables[] = $aevaprefix . 'albums';

	// The aeva_settings table
	db_query("
		CREATE TABLE IF NOT EXISTS {$aevaprefix}settings (
			name VARCHAR(30) PRIMARY KEY NOT NULL DEFAULT '',
			value TEXT NOT NULL
		)
	",__FILE__,__LINE__);

	$created_tables[] = $aevaprefix . 'settings';

	// The aeva_variables table
	db_query("
		CREATE TABLE IF NOT EXISTS {$aevaprefix}variables (
			id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
			type VARCHAR(15) NOT NULL DEFAULT '',
			val1 TEXT NOT NULL,
			val2 TEXT NOT NULL,
			val3 TEXT NOT NULL,
			val4 TEXT NOT NULL,
			val5 TEXT NOT NULL,
			val6 TEXT NOT NULL,
			val7 TEXT NOT NULL,
			val8 TEXT NOT NULL,
			val9 TEXT NOT NULL
		)
	",__FILE__,__LINE__);

	$created_tables[] = $aevaprefix . 'variables';

	// The aeva_comments table
	db_query("
		CREATE TABLE IF NOT EXISTS {$aevaprefix}comments (
			id_comment INT PRIMARY KEY AUTO_INCREMENT  NOT NULL,
			id_member INT NOT NULL DEFAULT 0,
			id_media INT NOT NULL DEFAULT 0,
			id_album INT NOT NULL DEFAULT 0,
			message TEXT NOT NULL,
			posted_on INT NOT NULL DEFAULT 0,
			last_edited INT NOT NULL DEFAULT 0,
			last_edited_by INT NOT NULL DEFAULT 0,
			last_edited_name VARCHAR(25) NOT NULL DEFAULT '',
			approved TINYINT(1) NOT NULL DEFAULT 0
		)
	",__FILE__,__LINE__);

	$created_tables[] = $aevaprefix . 'comments';

	// The aeva_log_media table
	db_query("
		CREATE TABLE IF NOT EXISTS {$aevaprefix}log_media (
			id_media INT NOT NULL DEFAULT 0,
			id_member INT NOT NULL DEFAULT 0,
			time INT NOT NULL DEFAULT 0,
			PRIMARY KEY (id_media, id_member)
		)",__FILE__,__LINE__);

	$created_tables[] = $aevaprefix . 'log_media';

	// The aeva_log_ratings table
	db_query("
		CREATE TABLE IF NOT EXISTS {$aevaprefix}log_ratings (
			id_media INT NOT NULL DEFAULT 0,
			id_member INT NOT NULL DEFAULT 0,
			rating TINYINT(1) NOT NULL DEFAULT 0,
			time INT NOT NULL DEFAULT 0,
			PRIMARY KEY (id_media, id_member)
		)
	",__FILE__,__LINE__);
	$created_tables[] = $aevaprefix . 'log_ratings';

	// The aeva_permissions table
	db_query("
		CREATE TABLE IF NOT EXISTS {$aevaprefix}perms (
			id_group INT NOT NULL DEFAULT 0,
			id_profile INT NOT NULL DEFAULT 0,
			permission VARCHAR(255) NOT NULL DEFAULT '',
			PRIMARY KEY (id_group, id_profile, permission)
		)
	",__FILE__,__LINE__);
	$created_tables[] = $aevaprefix . 'perms';

	// The permissions table
	db_query("
		CREATE TABLE IF NOT EXISTS {$aevaprefix}quotas (
			id_profile INT NOT NULL DEFAULT 0,
			id_group INT NOT NULL DEFAULT 0,
			type VARCHAR(10) NOT NULL DEFAULT '',
			quota INT NOT NULL DEFAULT 0,
			PRIMARY KEY (id_profile, id_group, type)
		)
	",__FILE__,__LINE__);
	$created_tables[] = $aevaprefix . 'quotas';

	// The custom fields table
	db_query("
		CREATE TABLE IF NOT EXISTS {$aevaprefix}fields (
			id_field INT NOT NULL AUTO_INCREMENT PRIMARY KEY,
			name VARCHAR(100) NOT NULL DEFAULT '',
			type VARCHAR(20) NOT NULL DEFAULT 'text',
			options TEXT NOT NULL,
			required TINYINT(1) NOT NULL DEFAULT 0,
			searchable TINYINT(1) NOT NULL DEFAULT 0,
			description TEXT NOT NULL,
			bbc TINYINT(1) NOT NULL DEFAULT 0,
			albums TEXT NOT NULL
		)",__FILE__,__LINE__);
	$created_tables[] = $aevaprefix . 'fields';

	// Custom fields' data table
	db_query("
		CREATE TABLE IF NOT EXISTS {$aevaprefix}field_data (
			id_field INT NOT NULL DEFAULT 0,
			id_media INT NOT NULL DEFAULT 0,
			value TEXT NOT NULL,
			PRIMARY KEY (id_media, id_field)
		)",__FILE__,__LINE__);
	$created_tables[] = $aevaprefix . 'field_data';

	// Member table alterations
	$request = db_query("SHOW FIELDS FROM {$db_prefix}members",__FILE__,__LINE__);
	$fields = array();
	while ($row = mysql_fetch_assoc($request))
		$fields[] = $row['Field'];
	mysql_free_result($request);

	// If mgal_* fields are in there, rename them.
	if (!in_array('aeva_items', $fields))
	{
		db_query("ALTER TABLE {$db_prefix}members " . (in_array('mgal_total_items', $fields) ? 'CHANGE mgal_total_items' : 'ADD') . " aeva_items INT NOT NULL DEFAULT '0'",__FILE__,__LINE__);
		$altered_mem[1] = true;
	}
	if (!in_array('aeva_comments', $fields))
	{
		db_query("ALTER TABLE {$db_prefix}members " . (in_array('mgal_total_comments', $fields) ? 'CHANGE mgal_total_comments' : 'ADD') . " aeva_comments INT NOT NULL DEFAULT '0'",__FILE__,__LINE__);
		$altered_mem[2] = true;
	}
	if (!in_array('aeva_unseen', $fields))
	{
		db_query("ALTER TABLE {$db_prefix}members " . (in_array('mgal_unseen', $fields) ? 'CHANGE mgal_unseen' : 'ADD') . " aeva_unseen INT NOT NULL DEFAULT '-1'",__FILE__,__LINE__);
		$altered_mem[3] = true;
	}

	// I'd rather use a TEXT field, but if SELECT @@sql_mode returns a strict mode, it may cause issues outside the mod itself...
	if (!in_array('aeva', $fields))
	{
		db_query("ALTER TABLE {$db_prefix}members ADD aeva VARCHAR(255) NOT NULL DEFAULT ''",__FILE__,__LINE__);
		$altered_mem[4] = true;
	}

	// Files table alterations
	$request = db_query("SHOW FIELDS FROM {$aevaprefix}files",__FILE__,__LINE__);
	$fields = array();
	while ($row = mysql_fetch_assoc($request))
		$fields[] = $row['Field'];
	mysql_free_result($request);

	if (!in_array('exif', $fields))
		db_query("ALTER TABLE {$aevaprefix}files ADD exif TEXT NOT NULL",__FILE__,__LINE__);

	// Fields table alterations
	$request = db_query("SHOW FIELDS FROM {$aevaprefix}fields",__FILE__,__LINE__);
	$fields = array();
	while ($row = mysql_fetch_assoc($request))
		$fields[] = $row['Field'];
	mysql_free_result($request);

	if (!in_array('description', $fields))
		db_query("ALTER TABLE {$aevaprefix}fields CHANGE `desc` description TEXT NOT NULL",__FILE__,__LINE__);

	// Quotas table alterations
	$request = db_query("SHOW FIELDS FROM {$aevaprefix}quotas",__FILE__,__LINE__);
	$fields = array();
	while ($row = mysql_fetch_assoc($request))
		$fields[] = $row['Field'];
	mysql_free_result($request);

	if (!in_array('quota', $fields))
		db_query("ALTER TABLE {$aevaprefix}quotas CHANGE `limit` quota INT NOT NULL DEFAULT 0",__FILE__,__LINE__);

	// Albums table alterations
	$request = db_query("SHOW FIELDS FROM {$aevaprefix}albums",__FILE__,__LINE__);
	$fields = array();
	while ($row = mysql_fetch_assoc($request))
		$fields[] = $row['Field'];
	mysql_free_result($request);

	if (!in_array('options', $fields))
		db_query("ALTER TABLE {$aevaprefix}albums ADD options TEXT NOT NULL",__FILE__,__LINE__);
	if (!in_array('id_perm_profile', $fields))
		db_query("ALTER TABLE {$aevaprefix}albums ADD id_perm_profile INT NOT NULL DEFAULT 0",__FILE__,__LINE__);
	if (!in_array('id_quota_profile', $fields))
		db_query("ALTER TABLE {$aevaprefix}albums ADD id_quota_profile INT NOT NULL DEFAULT 0",__FILE__,__LINE__);
	if (!in_array('hidden', $fields))
		db_query("ALTER TABLE {$aevaprefix}albums ADD hidden TINYINT(1) NOT NULL DEFAULT 0",__FILE__,__LINE__);
	if (!in_array('allowed_members', $fields))
		db_query("ALTER TABLE {$aevaprefix}albums ADD allowed_members VARCHAR(255) NOT NULL DEFAULT ''",__FILE__,__LINE__);
	if (!in_array('allowed_write', $fields))
		db_query("ALTER TABLE {$aevaprefix}albums ADD allowed_write VARCHAR(255) NOT NULL DEFAULT ''",__FILE__,__LINE__);
	if (!in_array('denied_members', $fields))
		db_query("ALTER TABLE {$aevaprefix}albums ADD denied_members VARCHAR(255) NOT NULL DEFAULT ''",__FILE__,__LINE__);
	if (!in_array('denied_write', $fields))
		db_query("ALTER TABLE {$aevaprefix}albums ADD denied_write VARCHAR(255) NOT NULL DEFAULT ''",__FILE__,__LINE__);
	if (!in_array('id_topic', $fields))
		db_query("ALTER TABLE {$aevaprefix}albums ADD id_topic INT NOT NULL DEFAULT 0",__FILE__,__LINE__);
	if (!in_array('bigicon', $fields))
		db_query("ALTER TABLE {$aevaprefix}albums ADD bigicon INT NOT NULL DEFAULT 0",__FILE__,__LINE__);
	if (!in_array('access_write', $fields))
		db_query("ALTER TABLE {$aevaprefix}albums ADD access_write VARCHAR(255) NOT NULL DEFAULT ''",__FILE__,__LINE__);
	if (!in_array('master', $fields))
	{
		db_query("ALTER TABLE {$aevaprefix}albums ADD master INT NOT NULL DEFAULT 0",__FILE__,__LINE__);
		db_query("ALTER TABLE {$aevaprefix}albums ADD INDEX id_master (master)",__FILE__,__LINE__);
		db_query("UPDATE {$aevaprefix}albums SET master = id_album WHERE parent = 0",__FILE__,__LINE__);
		$alb = array();
		$continue = true;
		$unstick = 0;
		while ($continue)
		{
			// This may very well crash on SQLite and maybe PGSQL... Ah, who cares?
			db_query("
				UPDATE {$aevaprefix}albums AS a1, {$aevaprefix}albums AS a2
				SET a1.master = a2.master
				WHERE (a1.parent = a2.id_album) AND (a1.master = 0) AND (a2.master != 0)",__FILE__,__LINE__);
			$continue = (db_affected_rows() > 0) && ($unstick++ < 100);
		}
	}
	if (!in_array('featured', $fields))
	{
		// Retrieve all non-admin primary groups used by members...
		$request = db_query("SELECT ID_GROUP FROM {$db_prefix}members GROUP BY ID_GROUP ORDER BY ID_GROUP",__FILE__,__LINE__);
		while ($row = mysql_fetch_row($request))
			$primary_groups[(int) $row[0]] = (int) $row[0];
		mysql_free_result($request);
		unset($primary_groups[1]);

		db_query("ALTER TABLE {$aevaprefix}albums ADD featured TINYINT(1) NOT NULL DEFAULT 0",__FILE__,__LINE__);
		db_query("
			UPDATE {$aevaprefix}albums
			SET featured = 1, album_of = 1, access_write = '" . implode(',', $primary_groups) . "'
			WHERE type = 'general'",
			__FILE__,__LINE__
		);
		db_query("ALTER TABLE {$aevaprefix}albums ADD INDEX id_of (id_album, album_of, featured)",__FILE__,__LINE__);
		db_query("ALTER TABLE {$aevaprefix}albums DROP COLUMN `type`",__FILE__,__LINE__);
	}
	else
		db_query("UPDATE {$aevaprefix}albums SET album_of = 1 WHERE album_of = 0",__FILE__,__LINE__);
	if (in_array('member_name', $fields))
		db_query("ALTER TABLE {$aevaprefix}albums DROP COLUMN `member_name`",__FILE__,__LINE__);

	$request = db_query("SHOW KEYS FROM {$db_prefix}aeva_media",__FILE__,__LINE__);
	$fields = array();
	while ($row = mysql_fetch_assoc($request))
		$fields[$row['Key_name']] = true;
	mysql_free_result($request);

	if (!isset($fields['id_thumb']))
		db_query("ALTER TABLE {$db_prefix}aeva_media ADD INDEX id_thumb (id_thumb)",__FILE__,__LINE__);
	if (!isset($fields['time_added']))
		db_query("ALTER TABLE {$db_prefix}aeva_media ADD INDEX time_added (time_added)",__FILE__,__LINE__);
	if (!isset($fields['album_id']))
		db_query("ALTER TABLE {$db_prefix}aeva_media ADD INDEX album_id (album_id)",__FILE__,__LINE__);

	// Permissions processing...
	if (!in_array($db_prefix . 'aeva_perms', $tables) && !in_array($db_prefix . 'mgallery_perms', $tables))
	{
		// Insert a brand new profile
		db_query("
			INSERT IGNORE INTO {$aevaprefix}variables
			(type, val1)
			VALUES
			('perm_profile', 'Default')",__FILE__,__LINE__);
		$id_profile = db_insert_id();

		// Bypass to a small issue
		if ($id_profile == 1)
		{
			$id_profile = 2;
			db_query("
				UPDATE {$aevaprefix}variables
				SET id = 2
				WHERE id = 1",
				__FILE__,__LINE__
			);
			db_query("ALTER TABLE {$aevaprefix}variables AUTO_INCREMENT = 3",__FILE__,__LINE__);
		}

		// Get existing permissions
		$request = db_query("
			SELECT permission, addDeny AS add_deny, ID_GROUP AS id_group
			FROM {$db_prefix}permissions",
			__FILE__,__LINE__);
		$removals = array();
		$perms = array();
		while ($row = mysql_fetch_assoc($request))
		{
			if (!in_array($row['permission'], array('aeva_download_item', 'aeva_add_videos', 'aeva_add_audios', 'aeva_add_images', 'aeva_add_embeds', 'aeva_add_docs', 'aeva_rate_items', 'aeva_edit_own_com', 'aeva_edit_own_item', 'aeva_comment', 'aeva_report_item', 'aeva_report_com', 'aeva_auto_approve_com', 'aeva_auto_approve_item', 'aeva_multi_upload', 'aeva_multi_download', 'aeva_whoratedwhat')))
				continue;

			if (!isset($perms[$row['id_group']]))
				$perms[$row['id_group']] = array();
			if (!isset($removals[$row['id_group']]))
				$removals[$row['id_group']] = array();

			if (empty($row['add_deny']))
				$removals[$row['id_group']][] = $row['permission'];
			else
				$perms[$row['id_group']][] = $row['permission'];
		}
		mysql_free_result($request);

		if (!empty($modSettings['permission_enable_deny']))
			foreach ($perms as $group => $permarray)
				$perms[$group] = array_diff($perms[$group], $removals[$group]);

		// Insert it to the profile
		foreach ($perms as $group => $permArray)
			foreach ($permArray as $perm)
				db_query("
					INSERT IGNORE INTO {$aevaprefix}perms
					(id_profile, id_group, permission)
					VALUES
					($id_profile, $group, '$perm')",__FILE__,__LINE__);

		// Update the albums
		db_query("
			UPDATE {$aevaprefix}albums
			SET id_perm_profile = $id_profile
			WHERE featured = 1",__FILE__,__LINE__
		);
		db_query("
			UPDATE {$aevaprefix}albums
			SET id_perm_profile = 1
			WHERE featured = 0",__FILE__,__LINE__
		);
	}

	// Media table alterations
	$request = db_query("SHOW FIELDS FROM {$aevaprefix}media",__FILE__,__LINE__);
	$fields = array();
	while ($row = mysql_fetch_assoc($request))
		$fields[] = $row['Field'];
	mysql_free_result($request);

	if (!in_array('id_preview', $fields))
	{
		db_query("ALTER TABLE {$aevaprefix}media ADD id_preview INT NOT NULL DEFAULT '0'",__FILE__,__LINE__);
		db_query("ALTER TABLE {$aevaprefix}media CHANGE type type VARCHAR(10) NOT NULL DEFAULT 'image'",__FILE__,__LINE__);
	}

	if (!in_array('downloads', $fields))
	{
		db_query("ALTER TABLE {$aevaprefix}media ADD downloads INT NOT NULL DEFAULT '0'",__FILE__,__LINE__);
		db_query("ALTER TABLE {$aevaprefix}albums CHANGE access access VARCHAR(255) NOT NULL DEFAULT ''",__FILE__,__LINE__);
	}

	if (!in_array('weighted', $fields))
		db_query("ALTER TABLE {$aevaprefix}media ADD weighted FLOAT NOT NULL DEFAULT '0'",__FILE__,__LINE__);

	db_query("ALTER TABLE {$aevaprefix}media CHANGE title title VARCHAR(255) NOT NULL DEFAULT ''",__FILE__,__LINE__);
	db_query("ALTER TABLE {$aevaprefix}albums CHANGE name name VARCHAR(255) NOT NULL DEFAULT ''",__FILE__,__LINE__);
	db_query("ALTER TABLE {$aevaprefix}settings CHANGE value value TEXT NOT NULL",__FILE__,__LINE__);

	// Insert the settings now
	$setting_entries = array();
	foreach ($newsettings as $name => $value)
	{
		$value = addslashes($value);
		db_query("INSERT IGNORE INTO {$aevaprefix}settings (name, value) VALUES ('$name', '$value')",__FILE__,__LINE__);
		if (db_affected_rows() > 0)
			$setting_entries[] = $name;
	}

	// Insert the mandatory data
	db_query("DELETE FROM {$aevaprefix}files WHERE id_file <= 4",__FILE__,__LINE__);
	db_query("DELETE FROM {$aevaprefix}settings WHERE name = 'doc_files'",__FILE__,__LINE__);

	db_query("
		INSERT INTO {$aevaprefix}files
		(id_file, filename, filesize, directory, width, height, id_album, exif)
		VALUES
		(1, 'music.png', 4118, 'generic_images', 48, 48, 0, '')",__FILE__,__LINE__);
	db_query("
		INSERT INTO {$aevaprefix}files
		(id_file, filename, filesize, directory, width, height, id_album, exif)
		VALUES
		(2, 'film.png', 2911, 'generic_images', 48, 48, 0, '')",__FILE__,__LINE__);
	db_query("
		INSERT INTO {$aevaprefix}files
		(id_file, filename, filesize, directory, width, height, id_album, exif)
		VALUES
		(3, 'camera.png', 2438, 'generic_images', 48, 48, 0, '')",__FILE__,__LINE__);
	db_query("
		INSERT INTO {$aevaprefix}files
		(id_file, filename, filesize, directory, width, height, id_album, exif)
		VALUES
		(4, 'folder.png', 2799, 'generic_images', 48, 48, 0, '')",__FILE__,__LINE__);

	$request = db_query("SELECT value FROM {$aevaprefix}settings WHERE name='data_dir_path'",__FILE__,__LINE__);
	list($data_dir) = mysql_fetch_row($request);
	$data_dir .= '/generic_images/';
	$ex_data_dir = $boarddir . '/mgal_data/generic_images/';
	$cam = $data_dir . 'camera.png';
	if ((!file_exists($cam) || filesize($cam) == 665) && filesize($ex_data_dir . 'camera.png') == 2438)
	{
		@copy($ex_data_dir . 'camera.png', $data_dir . 'camera.png');
		@copy($ex_data_dir . 'film.png', $data_dir . 'film.png');
		@copy($ex_data_dir . 'music.png', $data_dir . 'music.png');
		@copy($ex_data_dir . 'folder.png', $data_dir . 'folder.png');
	}
	mysql_free_result($request);

	require_once($sourcedir . '/Aeva-Subs-Vital.php');
	if (function_exists('aeva_allowed_types'))
	{
		$aty = aeva_allowed_types();
		$aty['do'][] = 'default';
		foreach ($aty['do'] as $ty)
			if (!file_exists($data_dir . $ty . '.png') && file_exists($ex_data_dir . $ty . '.png'))
				@copy($ex_data_dir . $ty . '.png', $data_dir . $ty . '.png');
	}
}

// OK, time to report, output all the stuff to be shown to the user
echo '
<table cellpadding="0" cellspacing="0" border="0" class="tborder" width="550" align="center"><tr><td>
<div class="titlebg" style="padding: 1ex">
	Aeva Media Database Installer
</div>
<div class="windowbg2" style="padding: 2ex">';

// Tell them what has been done
echo '<b>Creating / Updating Tables</b>
<br />
<ul class="normallist">';
$my_db_prefix = preg_replace('/`[^`]+`\./', '', $db_prefix);
foreach ($created_tables as $table_name)
{
	$table_name = str_replace('{db_prefix}', $db_prefix, $table_name);
	if (in_array($table_name, $tables))
		echo '
	<li>Table <i>'.$table_name.'</i> already exists.</li>';
	else
		echo '
	<li>Table <i>'.$table_name.'</i> created.</li>';
}
if (isset($altered_mem[1]))
	echo '
	<li>Altered '.$my_db_prefix.'members table, added field "aeva_items".</li>';
if (isset($altered_mem[2]))
	echo '
	<li>Altered '.$my_db_prefix.'members table, added field "aeva_comments".</li>';
if (isset($altered_mem[3]))
	echo '
	<li>Altered '.$my_db_prefix.'members table, added field "aeva_unseen".</li>';
if (isset($altered_mem[4]))
	echo '
	<li>Altered '.$my_db_prefix.'members table, added field "aeva" (stores user settings).</li>';

echo '
</ul>

<b>Initializing settings</b><br />
<ul class="normallist">';

if (count($setting_entries) == count($newsettings))
	echo '
	<li>All ', count($setting_entries), ' records have been inserted into the settings table.</li>';
else
	echo '
	<li>', empty($setting_entries) ? 'No' : count($setting_entries), ' new record(s) have been inserted into the settings table.</li>', !empty($setting_entries) ? '
	<li>Full list: <span style="color: #888">' . implode(', ', $setting_entries) . '</span></li>' : '';

echo '
</ul>
<div style="padding-top: 25px">
	<span style="font-weight: bold; color: green">Your database update has been completed successfully!</span>

	<br /><br /><strong>If this is the first time you install the gallery</strong>, you should now go to the Admin area, Members section, and head to the
	<a href="', $scripturl, '?action=admin;area=aeva_perms;', $context['is_smf2'] ? $context['session_var'] : 'sesc', '=', $context['session_id'], '" style="text-decoration: underline">Aeva Media Permissions</a>
	section. Create and manage your permission profiles and apply them to your albums. No one will be able to access the gallery until you enable permissions.

	<br /><br /><strong>If you\'re experiencing errors</strong> "Method Not Implemented", "403" or "406" when using Aeva Media, you probably need to disable mod_security (an Apache module). Please open your package file and read the instructions in the <strong>mod_security.htaccess</strong> file.
	<br /><br />If it doesn\'t work for you, you\'re out of luck. Ask your host to help you, or disable whatever feature doesn\'t work.';

if ($doing_manual_install && (is_writable(dirname(__FILE__)) || is_writable(__FILE__)))
	echo '
	<br /><br />
	<label for="delete_self"><input type="checkbox" id="delete_self" onclick="doTheDelete(this);" /> Delete this file.</label> <i>(doesn\'t work on all servers.)</i>
	<script language="javascript" type="text/javascript"><!-- // --><![CDATA[
		function doTheDelete(theCheck)
		{
			var theImage = document.getElementById ? document.getElementById("delete_upgrader") : document.all.delete_upgrader;

			theImage.src = "', $_SERVER['PHP_SELF'], '?delete=1&ts_" + (new Date().getTime());
			theCheck.disabled = true;
		}
	// ]]></script>
	<img src="', $boardurl, '/Themes/default/images/blank.gif" alt="" id="delete_upgrader" />';

echo '
	<br /><br /><b>Thank you for trying out Aeva Media!</b>
</div>
<div style="font-size: 9px; margin-top: 20px;" align="center">
	Aeva Media &copy; <a href="http://noisen.com/">noisen</a> / <a href="http://smf-media.com">smf-media</a>
</div>
</div>
</td></tr></table>
<br />';

if ($doing_manual_install)
	echo '
</body></html>';

?>
