<?php

function aeva_item_widgets()
{
	global $galurl, $context, $amSettings, $txt, $scripturl, $settings, $boardurl, $user_info, $amFunc;

	$item = $context['item_data'];

	echo '
		<div class="cat_bar"><h3>Basic Stats</h3></div><div class="roundframe"><dl class="settings">
			<dt>', $txt['aeva_posted_on'], '</dt><dd>', timeformat($item['time_added']), '</dd>';

	if ($item['type'] != 'embed')
		echo !empty($item['width']) && !empty($item['height']) ? '
			<dt>' . $txt['aeva_width'] . '&nbsp;&times;&nbsp;' . $txt['aeva_height'] . '</dt><dd>' . $item['width'] . '&nbsp;&times;&nbsp;' . $item['height'] . '</dd>' : '', '
			<dt>', $txt['aeva_filesize'], '</dt><dd>', $item['filesize'], ' ', $txt['aeva_kb'], '</dd>
			<dt>', $txt['aeva_filename'], '</dt><dd style="overflow: hidden;">', $item['filename'], '</dd>';

	if ((!empty($item['keyword_list']) && implode('', $item['keyword_list']) != '') || !empty($item['keywords']))
	{
		echo '
			<dt>', $txt['aeva_keywords'], '</dt><dd>';
		$tag_list = '';
		if (!empty($item['keyword_list']))
		{
			foreach ($item['keyword_list'] as $tag)
				if (!empty($tag))
					$tag_list .= '<b><a href="' . $galurl . 'sa=search;search=' . urlencode($tag) . ';sch_kw">' . $tag . '</a></b>, ';
		}
		else
			echo $item['keywords'];
		echo substr($tag_list, 0, strlen($tag_list) - 2) . '</dd>';
	}

	echo '
			<dt>', $txt['aeva_views'], '</dt><dd>', $item['views'], '</dd>', !empty($item['downloads']) ? '
			<dt>' . $txt['aeva_downloads'] . '</dt><dd>' . $item['downloads'] . '</dd>' : '', '
			</dl></div><div class="cat_bar"><h3>', $txt['aeva_rating'], '</h3></div><div class="roundframe" id="ratingElement">', template_aeva_rating_object($item), '</div>';

	foreach ($item['custom_fields'] as $field)
	{
		if (!empty($field['value']))
		{
			if ($field['name'] == 'Short Description')
				echo '
			<div class="cat_bar"><h3>Short Description</h3></div><div class="roundframe">', $field['value'], '</div>';
			elseif ($field['name'] == 'Supported Languages')
				echo '
			<div class="cat_bar"><h3>Supported Languages</h3></div><div class="roundframe">', str_replace(',', '', $field['value']), '<a href="#" onclick="expandLanguages(this); return false;" style="display: block; outline: none; margin-top: 0.25em;">(expand)</a></div><div class="cat_bar"><h3>Additional Information</h3></div><div class="roundframe"><dl class="settings">';
			else
			{
				echo '
			<dt>', $field['name'], '</dt><dd style="overflow: hidden;">';
				if ($field['searchable'])
				{
					$build_list = '';
					foreach ($field['value'] as $val)
						$build_list .= '<a href="' . $galurl . 'sa=search;search=' . urlencode($val) . ';fields[]=' . $field['id'] . '">' . $amFunc['htmlspecialchars']($val) . '</a>, ';
					echo substr($build_list, 0, -2);
					unset($build_list);
				}
				else
					echo substr($field['value'], 0, 7) == 'http://' ? '<a href="' . $field['value'] . '">' . $amFunc['htmlspecialchars']($field['value']) . '</a>' : $field['value'];

				echo '</dd>';
			}
			$no_fields = true;
		}
	}

	if (!isset($no_fields))
		echo '
			<div class="roundframe"><dl class="settings">';

	if ($amSettings['show_linking_code'])
	{
		echo '
			<dt>', $txt['aeva_embed_bbc'], '</dt><dd>
					<input id="bbc_embed" type="text" style="width: 90%;" value="[smg id=' . $item['id_media'] . ($item['type'] == 'image' ? '' : ' type=av') . ']" onclick="return selectText(this);" readonly="readonly" />
					<a href="', $scripturl, '?action=media;sa=smgtaghelp" onclick="return reqWin(this.href, 600, 400);"><img alt="" src="', $settings['images_url'], '/helptopics.gif" class="vam" border="0" /></a>
				</dd>';

		// Don't show html/direct links if the helper file was deleted
		if ($amSettings['show_linking_code'] == 1)
		{
			if (strpos($item['embed_object'], 'swfobject.embedSWF') === false)
				echo '
			<dt>', $txt['aeva_embed_html'], '</dt><dd>
					<input id="html_embed" type="text" style="width: 90%;" value="', $item['type'] == 'image' ?
						htmlspecialchars('<img src="' . $boardurl . '/MGalleryItem.php?id=' . $item['id_media'] . '" alt="" />') :
						htmlspecialchars(trim(preg_replace('/[\t\r\n]+/', ' ', $item['embed_object']))), '" onclick="return selectText(this);" readonly="readonly" />
				</dd>';
			if ($item['type'] != 'embed')
				echo '
			<dt>' . $txt['aeva_direct_link'] . '</dt><dd>
					<input id="direct_link" type="text" style="width: 90%;" value="' . $boardurl . '/MGalleryItem.php?id=' . $item['id_media'] . '" onclick="return selectText(this);" readonly="readonly" />
				</dd>';
		}
	}
	echo '
			</dl></div>';
}