
				   Aeva Media:
				License Agreement
				 March 20, 2010
			� noisen.com/smf-media.com

Definitions
-----------
1. "AM" and "AeMe" stand as the short forms for "Aeva Media", and "Noisen.com" and
   "SMF-Media.com" as the official websites for Aeva Media and its associated packages.
2. "The package" stands for all files within any archive file or any group of files related
   to AeMe and released through or on behalf of Noisen.com and/or SMF-Media.com
3. The main copyright holder is defined as Ren�-Gilles Deberdt ("Nao"). Shitiz Garg ("Dragooon")
   also owns part of the Gallery section's source code, and retains his rights on it.
4. "� Noisen.com & SMF-Media.com" automatically refers to the aforementioned current copyright holders.
5. "Distribution" means allowing one or more other people to download or receive in any way
   a copy of the package, or part of it, free of charge or for a price.
6. "A modification" is defined as an instruction, or set of instructions, to need to be performed
   manually or automatically, and that will alter any part of the package.
7. 3rd-party packages are defined as packages obtained through 3rd-party sources, and are neither
   written, nor published by the copyright holders of AeMe.

Disclaimer
----------
This package is provided "as is" and without any warranty. Any express or implied warranties,
including, but not limited to, the implied warranties of merchantability and fitness for a
particular purpose are disclaimed. In no event shall the authors and/or copyright holders
be liable to any party for any direct, indirect, incidental, special, exemplary, or
consequential damages arising in any way out of the use or misuse of the package.

Agreement
---------
1. All copyright notices in source files or otherwise generated shall remain intact without
   modification, unless being given written permission from the copyright holders. The name
   of the package and links to associated websites must remain in clear view for all users.
2. Distribution of this package is not allowed unless being given written permission
   from the copyright holders.
3. You may modify the package, or one or more of its files, to the extent that you remain
   in full compliance of the aforementioned clauses.
4. Distribution of modifications is allowed, to the extent that you remain in full compliance
   of the aforementioned clauses.
5. The copyright holders retain the right to change this agreement, or parts of it,
   without any prior notice, at any time. It is the user's responsibility to make sure
   their knowledge of the license agreement is up to date. The copyright holders will
   communicate any changes through the Noisen.com or SMF-Media.com websites.
6. Upon neglecting one or more clauses of the above, the license will automatically terminate.
   Upon termination the user must destroy all of their copies of the package within 24 hours.
   The copyright holders shall take action against any license agreement breach at their discretion.
7. All ownership & contractual rights of any 3rd-party package that may be provided with this package
   remain in possession of their respective copyright holders. The AeMe copyright holders do not
   retain any ownership or rights over them, unless written otherwise, for instance in the case
   any 3rd-party package uses parts of the AeMe package's source code.
8. You are not allowed to sell or lease the modification, or part thereof. You are not allowed
   to sell anything based on the modification, or code that requires the modification to be installed,
   without the copyright holders' written consent.
9. You shall not install and/or use Aeva Media on a website that encourages and/or endorses
   violence, hate and/or any kind of bigotry. Be nice to each other!
10. THIS IS NOT A PRODUCT. THIS IS A LABOR OF LOVE. You may neither call Aeva Media a "product", nor
    request to be considered as a "customer". (This does not apply to the Foxy! add-on software,
	although the author would still appreciate that you do not consider it a "product".)
11. Which means if you are not happy with it, your only right is to uninstall it. You may not demand
    any support, help or consideration regarding your issues. If you want help, ask for it nicely.
    Remember that no one is getting paid for this. You may get help, but you must not expect it.
12. Insulting the authors or starting fights on discussion topics related to Aeva Media *may* result
    in the authors terminating your right to use the Aeva Media software. Failure to comply to any
    other rule in the agreement *will* automatically terminate your right to use the AeMe software.

Just so that we're clear on this: if you don't like the software or its authors, don't use it.
